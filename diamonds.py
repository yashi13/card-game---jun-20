def legalCount(card_value):
    value_to_count = {'A' : 14, 'K' : 13, 'Q' : 12, 'J' : 11 , '10' : 10,  '9' : 9,  '8' : 8,  '7' : 7,  '6' : 6,  '5' : 5,  '4' : 4, '3' : 3,  '2' : 2}
    return value_to_count[card_value]
    
def isLegalMove(move, move_list):
    return move in move_list